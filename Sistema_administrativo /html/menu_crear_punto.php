<?php
session_start();
include("../conf.php");
include("../php/lib/conexion.php");
$con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Insertar_Informacion']==1)

{
    
    $db=$_CONF['db_mysql'];
    $sql="SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES
         WHERE TABLE_SCHEMA = '$db' AND   TABLE_NAME   = 'punto_libre';";
  
  $resp=mysql_query($sql,$con);
  if(!$resp){
      echo "Error hubo un problema";
      echo mysql_error;
      exit();
  }else{
    $id=null;  
    while($fila=mysql_fetch_assoc($resp)){
      
      $id=$fila['AUTO_INCREMENT'];
      
    }
    
  
  }
  
  ?>
  

  

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>crear punto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
	
	
		<script type="text/javascript" src="../js/validadores.js"></script>

  </head>

  <body>
 <form  action="../php/crearpunto.php" method="post"
                      enctype="multipart/form-data"  name="nombreForm" id="form"> 
 
    <?php
        include("cabecera.php")
      ?>                              
           <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav  affix"">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active" title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
		    <div class="span9">
		     <div class="hero-unit">
				   <h3 class="text-center">Planilla para crear un nuevo Punto Libre</h3>
				    <table id="tabla">
						   
						
					<div class="row-fluid">
						<div  class="span12 text-center btn-primary  ">
						      <span>Ubicación de nuevo punto libre</span>
						</div>
				       </div>					 
						<div class="row-fluid">
						      <div class="span12">
								<div class="span4">
								    <span>Codigo Punto</span>
								    <input type="text" id="codigopunto" name="codigopunto" readonly="" value="<?php echo $id; ?>" class="text-right" />
							       </div>
							    
							    <div class="span4">
								<span>Calendario</span>
								 <div class="control-group">
								      <div class="controls input-append date form_date" data-date="" data-date-format="yyyy-mm-dd" 
									    data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
									   <input size="16" type="text" name="fecha__creacion" value="" readonly required>
									   <span class="add-on"><i class="icon icon-calendar"></i></span>
								      </div>
								      <input type="hidden" id="dtp_input2" value="" /><br/>
								 </div>
							    </div>	      
							    <div class="span3 ">
						                <span>   Estado    </span><br>
							     <select  name="estado" required>	
								    <option value="">Seleccionar</option>}			
								     <option value="Amazonas">Amazonas</option>
									 <option value="Anzoategui">Anzoategui</option>
									 <option value="Apure"> Apure</option>
									 <option value="Aragua">Aragua</option>
									 <option value="Barinas">Barinas</option>
									 <option value="Bolívar">Bolívar	</option>
									 <option value="Carabobo">Carabobo</option>
									 <option value="Cojedes">Cojedes</option>
									 <option value="Delta Amacuro">Delta Amacuro</option>
									 <option value="Distrito Capital">Distrito Capital</option>
									 <option value="Guárico">Guárico	</option>
									 <option value="Lara">Lara</option>
									 <option value="Merida">Mérida</option>
									 <option value="Miranda">Miranda</option>
									 <option value="Monagas">Monagas</option>
									 <option value="Portuguesa">Portuguesa	</option>
									 <option value="Sucre">Sucre</option>
									 <option value="Tachira">Táchira	</option>
									 <option value="Trujillo">Trujillo</option>
									 <option  value="Vargas">Vargas</option>
									 <option  value="Yaracuy">Yaracuy	</option>
									 <option  value="Zulia">Zulia</option>
									 <option>......</option>
                                                            </select>					
							    </div>
						      </div>
					        </div><br>	
			                              <div class="row-fluid">
							   <div class="span12">
								<div class="span4">
									<span>Ciudad</span>
									<br>
									<select  name="ciudad" required>	
									    <option value="">Seleccionar</option>
									    <option value="Caracas">Caracas</option>
									    <option value="Maracaibo">Maracaibo</option>
									    <option value="Valencia">Valencia</option>
									    <option value="Apure">Apure</option>
									    <option value="Aragua">Aragua</option>
									    <option value="Barquisimeto">Barquisimeto</option>
									    <option value="San Cristóbal">San Cristóbal	</option>
									    <option value="Ciudad Guayana">Ciudad Guayana</option>
									    <option value="Barcelona">Barcelona</option>
									    <option value="Valera">Valera</option>
									    <option value="Maracay">Maracay</option>
									    <option value="Petare">Petare</option>
									    <option value="Turmero">Turmero</option>
									    <option value="Ciudad Bolívar">Ciudad Bolívar</option>
									    <option value="Barinas">Barinas</option>
									    <option value="Santa Teresa del Tuy">Santa Teresa del Tuy</option>
									    <option value="Cumaná">Cumana</option>
									    <option value="Baruta">Baruta</option>
									    <option value="Puerto la Cruz">Puerto la Cruz	</option>
									    <option value=">Mérida">Mérida</option>
									    <option value="Cabimas">Cabimas</option>
									    <option value="Coro">Coro</option>
									    <option value="Guatire">Guatire</option>
									    <option value="Cúa">Cúa	</option>
									    <option value="Guarenas">Guarenas</option>
									    <option value="Los Teques">Los Teques</option>
									    <option value="Ocumare del Tuy">Ocumare del Tuy</option>
									    <option value="Puerto Cabello">Puerto Cabello</option>
									    <option value="Guacara">Guacara</option>
									    <option value="El Tigre">El Tigre</option>
									    <option value="El Limón">El Limón</option>
									    <option value="Acarigua">Acarigua	</option>
									    <option value="Punto Fijo">Punto Fijo</option>
									    <option value="Cabudare">Cabudare</option>
									    <option value="Charallave">Charallave</option>
									    <option value="Palo Negro">Palo Negro</option>
									    <option value="Cagua">Cagua</option>
									    <option value="Anaco">Anaco</option>
									    <option value="Calabozo">Calabozo</option>
									    <option value="Guanare">Guanare</option>
									    <option value="Carúpano">Carúpano</option>
									    <option value="Ejido">Ejido</option>
									    <option value="Catia La Mar">Catia La Mar</option>
									    <option value="Mariara">Mariara</option>
									    <option value="">......</option>
							    
					                            </select>	
                                                                                              
							       </div>
								<div class="span4 ">
								      <span>Dirección</span>
								      <textarea  name="direccion" maxlength="150" cols="40" rows="3"
										 required></textarea>											    	
							       </div>
							  </div>
						       </div>					
						       <div class="row-fluid">
							     <div class="span12">
								    <span>    Croquis </span>
								    </br>
								    <p class="muted">Carga el croqui del Nuevo Punto:
								    <colspan="2"><input type="file" name="imagenes" id="imagenes" required >													
							     </div>
						     </div>
					       </div>
					    <div class="row-fluid">
						<div class="span12 text-center btn-primary">
							    <span>¿Quieres asignar el Encargado, Ayudante y Tecnico?</span>
							</div>
	                                        <div class="text-center">
						 Si<input type="radio" name="equipos" onclick="document.nombreForm.idSelect.disabled = false; document.nombreForm.idSelect1.disabled = false; document.nombreForm.idSelect2.disabled = false;
						 document.nombreForm.idSelect3.disabled = false; "/>
					         No<input type="radio" name="equipos" onclick="document.nombreForm.idSelect.disabled = true; document.nombreForm.idSelect1.disabled = true; document.nombreForm.idSelect2.disabled = true;
						 document.nombreForm.idSelect3.disabled = true;"/>
						</div>
						<br>
					    
						      <div class="row-fluid">
							<div class="span12 text-center btn-primary">
							    <span>Todas las personas que laboran en el punto</span>
							</div>
						    </div>
						    
						    </div>

     
						    <div class="row-fluid">	
							<div class="span12 text-center" id="">
								<span>Nombre del Encargado:</span> 
								<br/>
								<select name="id_usuario" required  id="idSelect">
									     <option value="">Seleccione...</option>
									       <?PHP
										 $busq_m=mysql_query("select usuarios.personas_idpersonas, personas.nombres, personas.apellidos, personas.cedula, usuarios.idusuarios from personas JOIN usuarios ON personas.idpersonas = usuarios.personas_idpersonas ORDER BY idusuarios");
										   while($reg_m=mysql_fetch_array($busq_m))
										   {
										   echo "<option value='".$reg_m['idusuarios']."' >".$reg_m['nombres']. "          ". $reg_m['apellidos']. " = C.I   ".$reg_m['cedula']."</option>";
										   }			
										?>
							       </select>
							</div>
						    </div>	
						     <div class="row-fluid">	
							
									<div class="span6 text-center"  >
										<span>Secretario(a):</span> 
										<br>
							        	<select name="id_usuario1" required  id="idSelect1">
									     <option value="">Seleccione...</option>
									       <?PHP
										 $busq_m=mysql_query("select usuarios.personas_idpersonas, personas.nombres, personas.apellidos, personas.cedula, usuarios.idusuarios from personas JOIN usuarios
										  ON personas.idpersonas = usuarios.personas_idpersonas ORDER BY idusuarios");
										   while($reg_m=mysql_fetch_array($busq_m))
										   {
										   echo "<option value='".$reg_m['idusuarios']."' >".$reg_m['nombres']. "          ". $reg_m['apellidos']. " = C.I   ".$reg_m['cedula']."</option>";
										   }			
										?>
							            </select>
							         </div>  
							            <div class="span6 text-center">
											<span>Ayudante:</span> 
											<br>
								            	<select name="id_usuario2" required  id="idSelect2">
										     <option value="">Seleccione...</option>
										       <?PHP
											 $busq_m=mysql_query("select usuarios.personas_idpersonas, personas.nombres, personas.apellidos, personas.cedula, usuarios.idusuarios from personas JOIN usuarios
											  ON personas.idpersonas = usuarios.personas_idpersonas ORDER BY idusuarios");
											   while($reg_m=mysql_fetch_array($busq_m))
											   {
											   echo "<option value='".$reg_m['idusuarios']."' >".$reg_m['nombres']. "          ". $reg_m['apellidos']. " = C.I   ".$reg_m['cedula']."</option>";
											   }			
											?>
							                       </select>
								    </div><div class="span12 text-center">
											<span>Tecnico:</span> 
											<br>
								            	<select name="id_usuario3" required  id="idSelect3">
										     <option value="">Seleccione...</option>
										       <?PHP
											 $busq_m=mysql_query("select usuarios.personas_idpersonas, personas.nombres, personas.apellidos, personas.cedula, usuarios.idusuarios from personas JOIN usuarios
											  ON personas.idpersonas = usuarios.personas_idpersonas ORDER BY idusuarios");
											   while($reg_m=mysql_fetch_array($busq_m))
											   {
											   echo "<option value='".$reg_m['idusuarios']."' >".$reg_m['nombres']. "          ". $reg_m['apellidos']. " = C.I   ".$reg_m['cedula']."</option>";
											   }			
											?>
							                       </select>
								    </div>	
				</table>
						<br>
						<br>
				    <div class="row-fluid">
                                        <div class="span12 text-center">
					    <div class="btn-group">
					       <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
					       <button type="submit"   class="btn btn-primary" ><i class="icon-file icon-white"></i> Enviar</button>
                                           </div>
                                      </div>
                                   </div>
                          </div>
           </div>
        </div>

      </div> 
  </div>
           
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
	<script type="text/javascript">
	//$("dropdown-toggle").dropdown{};
	       
	  </script>
	
	  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
	<script type="text/javascript">
	    $('#myModal').on('hidden', function () {
	  
          
          
	    })</script>
	        <script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
		<!--<script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>-->
		 
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script><!-- Script y js del calendario -->
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script> <!-- Script y js del calendario en español -->
    <script type="text/javascript">
	$('.form_date').datetimepicker
	({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 2,
		   minView: 2,
		   forceParse: 0
       });
	   $('.form_time').datetimepicker
	   ({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 1,
		   minView: 0,
		   maxView: 1,
		   forceParse: 0
       });
	</script>
 
</form>
</body>
</html>
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>

