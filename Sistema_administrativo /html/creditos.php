<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8" />
		<title>Creditos</title>
		 <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
	
</head>
<body>
		   <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li class="active"><a href="#"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i> Administración de P.L.<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="permisologia.php"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="consultausuarios.php"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="consultausuarios.php"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="#"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="menu_crear_punto.php"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                         
			  <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar y Status de los P.L</a></li>
                           <li><a href=""><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="#"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="#"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                                
            <br>
            <br>
            <!--
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active"><a href="#"><i class="icon-home icon-white"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.HTML" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.HTML"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li><a href="modificacion_de_los_puntos.HTML"><i class="icon-check"></i>Modificar y Deshabilitar los Puntos Libres</a></li>
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
                    -->
		<br><a id="top"></a>
		<div class="container-fluid">
			<div class="row text-center">
			
            <h2>Creditos</h2>
            <p>Este sistema fue realizado en la Universidad Politecnica Territorial del Estado Mérida (UPTMKR)</p>
            <strong><p>Bajo la Tutoria de:</p></strong>
         </div>
         <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Tutores Academicos</h4>
                </div>
                <div class="span6 well">
                    <h4>Tutores en la comunidad</h4>
                </div>
            </div>
         </div>
         <div class="row text-center">
            <strong><p>Fue desarrollado por:</p></strong>
         </div>
         <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Módulo: Administración de Punto Libre</h4>
                    <p>El Modulo de Administracion de Punto Libre fue desarrollado por:</p>
                      <div class="row-fluid thumbnail">
                              <div class="span4">
                                        <div class="thumbnail">
                                             <img src="../img/imag1.jpg" alt="hola">
                                         </div>
                                     <div class="caption">
                                          <h4>Ericka Simancas</h4>
                                          <a href="https://twitter.com/akciresima" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @akciresima</a>
                            							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            							<br>
                            							<a href="//plus.google.com/108178194666820594837?prsrc=3"
                            							   rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
                                     </div>
                              </div>
                                  <div class="span4">
                                      <div class="thumbnail">
                                         <img src="../img/img4.jpg" alt="hola">
                                      </div>
                                      <div class="caption">
                                          <h4>Joeinny Osorio</h4>
                                          <a href="https://twitter.com/joeinnyosorio1" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @joeinnyosorio1</a>
                                          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                      </div>
                                </div>
                                    <div class="span4">
                                      <div class="thumbnail">
                                         <img src="../img/img6.jpg" alt="hola">
                                      </div>
                                      <div class="caption">
                                          <h4>Nelly Ortiz</h4>
                                          <a href="https://twitter.com/nellyortiz" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @richard</a>
                                          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                      </div>
				           <div class="span4">
                                              <div class="thumbnail">
                                              <img src="../img/imag2.jpg" alt="hola">
                                          </div>
                                      <div class="caption">
                                          <h4>Richard Torres</h4>
                                          <a href="https://twitter.com/richardtorrez" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @richard</a>
                                          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                                      </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                <div class="span6 well">
                    <h4>Módulo: Gestión de Cursos</h4>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Módulo: Organización Inventario</h4>
                </div>
                <div class="span6 well">
                    <h4>Módulo: Gestión de Soporte a Equipos</h4>
                    <p>EL módulo de gestión de soporte a equipos fue desarrollado los estudiantes:</p>
                    <div class="row-fluid thumbnail">
                        <div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image10.png" alt="hola">
                            </div>
                            <div class="caption">
                            <h4>Jesús Vielma</h4>
                            <a href="https://twitter.com/chuyquien" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @chuyquien</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<br>
<a href="//plus.google.com/108178194666820594837?prsrc=3"
   rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
<span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Jesús Vielma</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
</a>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image11.png" alt="hola">
                            </div>
                            <div class="caption">
                            <h4>Engelbert Portillo</h4>
                            <a href="https://twitter.com/alexangelbert" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @alexangelbert</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image12.png" alt="hola">
                            </div>
                            <div class="caption">
                                <h4>Pedro Peralta</h4>
                                <a href="https://twitter.com/PetterV27" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @PetterV27</a>
                  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                        </div>
                    </div>
                </div>




               </div>
          </div>
     </div>
                
		<div class="row-fluid">
			<div class="span12">
				<div class="span8">
					<p class="muted"><a href="http://puntolibre.gnu.org.ve/" target="_blank"><img src="../img/punto-small.png"/> Punto Libre 2013 <i class="icon-globe"></i></a></p>
				</div>
				<!--<div class="span4">
					<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">
					<img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a>
					<br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">licencia de Creative Commons Reconocimiento-CompartirIgual 3.0 Unported</a>.
				</div>-->
				<div class="span4">
					<p class="muted pull-right"><a href="http://gnu.org.ve/" target="_blank"><img src="../img/gnu-small.png"/> Proyecto GNU Venezuela 2013 <i class="icon-globe"></i> </a></p>
				</div>
			</div>
		</div>
     
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>
	</body>
</body>
</html>