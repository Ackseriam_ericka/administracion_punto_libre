<?php
session_start();

include("lib/conexion.php");
include("../conf.php");
$con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Consultar_Informacion']==1)
{
?>	
	
	
<!DOCTYPE html>
<html lang="es">
<head>
       
        <meta charset="UTF-8">
	<title>Consulta de usuario</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">

<link rel="stylesheet" type="text/css" href="../css/main.css">

</head>
 <body>
  <form  action="<?php echo $_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php"; ?>" method="post">
	<head>
    <meta charset="utf-8" />
    <title>Sistema Administrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" media="screen"/>  
<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
  </head>

  <body>

 
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="../html/menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown active"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i>Administración<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="../html/permisologia.php"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="../html/consultausuarios.php"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="../html/consultausuarios.php"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="../html/consulta_general.php"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="../html/menu_crear_punto.php"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar Puntos Libres</a></li>
                           <li><a href="../html/ayuda.php"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="icon-user icon-white"></i><?php echo    $_SESSION['usuario']; ?> <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                          <li><a href="perfil.php"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
               </div>
                  </div>
                </div>
              </div>
            </div> 
	     <?php
	     
	      $_CONSUL=array();
	      
	      $cedula=$_POST['cedula'];
	  
	       $consul= "SELECT cedula FROM personas   WHERE (cedula='$cedula')";
	       $resp=mysql_query($consul);
	       if(!$resp){
		  //Error en la consulta
		  echo "Error en la consulta: ".mysql_error();
	       }else if(mysql_numrows($resp)==0){
		  
		  //No coincide el usuario y la contraseña
		//  $_CONSUL['error']="Cedula no encontrada en la base de datos";
		 
		// header("Location:". $_CONF['server_web'].$_CONF['app']."html/consultausuarios.php");
		   echo "<script type=text/javascript>
				    alert(' Cédula no existente en la base de datos. Presiona aceptar para continuar');
				    document.location=('../html/consultausuarios.php');
				</script>";
	    }else{
		    
		    
		    $registro=mysql_fetch_assoc($resp);
		    $_CONSUL['cedula']=True;
		    if($_CONSUL['cedula']==True)
		    
		    $_CONSUL['cedula']=$cedula;

	       
    
		    $orden="SELECT * FROM personas JOIN usuarios ON personas.idpersonas=usuarios.personas_idpersonas
				   WHERE cedula='$cedula';";
				   $listado = mysql_query ($orden,$con);
				   
				   echo mysql_error();
			     if ($lista= mysql_fetch_array($listado));
				  {
					  
						    
					  
					  echo"<br><br>
					  
                        <div class='span12'>
                            <div class='hero-unit'>
					  <div class='row-fluid'>
					      <div class='span12 text-center btn-primary'>
					     <span>Datos del usuario Solicitado</span>
					  </div>
						    </div>";
					  echo "<table width=20% class='table table-striped'class='span12'  border=4 bordercolor=#0080FF cellpadding=2 align=center>";
					  echo"<tr VALIGN=top class='success'><th>Nombres: </th><td> ".$lista["nombres"]."</td></tr>
						    <tr><th>Apellidos:  </th><td>".$lista["apellidos"]."</td></tr>
						    <tr><th>Cedula_identidad:  </td><td>".$lista["cedula"]."</td></tr>
						    <tr><th>Nombre_usuario: </th><td>".$lista["login"]."</td></tr>
						    <tr><th>sexo: </td><th>".$lista["sexo"]."</td></tr>
						    <tr><th>correo_electronico: </th><td>".$lista["email"]."</td></tr
						    <tr><th>numero_telefonico: </th><td>".$lista["telefono"]."</td></tr>
						    <tr><th>cargo_que_ocupa: </th><td>".$lista["cargo"]."</td></tr>";
						     while ($lista = mysql_fetch_array($listado)); 
					  echo "</table>";
				       
				  }
                             }          
	
    
 echo"</div>
      </div>"
		  
		  
   
		 
		  
	
  
		
		
//<button class="btn btn-danger" type="submit" href=paginaprincipal.php' ><i class=" icon-share-alt icon-white"></i> Regresar</button>
                   
        ?>
  <div class="row-fluid">  
   <div class="span12 text-center"> 
     <a class="btn btn-danger" href="../php/consulta_pdf1.php">Exportar pdf</a>
   </div>
  </div> 
	        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
  <script type="text/javascript">
  ${"dropdown-toggle"}.dropdown{}
         
    </script>
  

  
    <script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>
           
                          
</body>
</form>
</html>

<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
  
}
?>
		
		
		
		
		
		
	
		

























































