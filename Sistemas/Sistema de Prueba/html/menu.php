    <div class="container-fluid">
      <div class="row-fluid">
	<div class="span3">
	  <div class="well sidebar-nav">
	    <ul class="nav nav-pills nav-stacked">
	      <li class="nav-hearder"><h4>Control de Inventario</h4></li>
	      
		<li><a href="planilla_recepcion.php"><i class="icon-file"></i>Registro de Bienes</a></li>
	      <li class="dropdown ">
			<a class="dropdown-toggle" data-toggle="dropdown" href=""><i class="icon-search"></i>Consulta de Bienes<b class="caret"></b></a> 
			<ul class="dropdown-menu">
			   <li><a href="planilla7_porTipo.php"><i class="icon-search"></i>Consulta por Tipo</a></li>
		           <li><a href="planilla8_estatus.php"><i class="icon-search"></i>Consulta por Estatus</a></li>
			</ul>	
	          </li>
	      <li class="dropdown ">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
		<ul class="dropdown-menu ">
		  <li class="disabled"><a href="#">Reporte </a></li>
		  <li class="disabled"><a href="#">Respaldar</a></li>
		  <li class="disabled"><a href="#">Restaurar</a></li>
		  <li class="divider"></li>
		  <li><a href="#"><i class="icon-headphones"></i>Ayuda</a></li>
		</ul>	
	      </li>
	    </ul>
	  </div>
	</div>
    