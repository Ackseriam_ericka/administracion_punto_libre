<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"p
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>Sistema de Control de Inventario de Bienes Muebles</title>
    <meta name="autor" content="Maria Teran, Iris Diaz, Holgui Vega" />
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <style>
	body {padding-top: 50px;}
    </style>
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
	<div class="container-fluid">
	  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	  </button>
	  <a class="brand" href="#"><img src="../img/punto-small.png"/>Punto Libre</a>
	  <div class="nav-collapse collapse">
	    <ul class="nav">
	      <li><a href="#"><i class="icon-home"></i> Administración de P.L.</a></li>
	      <li><a href="#"><i class="icon-book"></i> Gestion de Cursos</a></li>
	      <li class="active dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-hdd"></i>Control de Inventario<b class="caret"></b></a>
		  <ul class="dropdown-menu">
		      <li><a href="planilla_recepcion.php" ><i class="icon-pencil"></i>Registro de Bienes </a></li>
		      <li><a href="planilla7_porTipo.php"><i class="icon-search "></i>Consulta por Tipo</a></li>
		      <li><a href="planilla8_estatus.php"><i class="icon-search "></i>Consulta por Estatus</a></li>
		  </ul>
	      </li>
	      <li><a href="#"><i class="icon-tasks"></i> Soporte a Equipos</a></li>
	    </ul>
	    <div class="pull-right">
	      <ul class="nav pull-right">
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
		  <ul class="dropdown-menu">
		      <li><a href="#"><i class="icon-user"></i>Perfil</a></li>
		      <li><a href="#"><i class="icon-headphones"></i>Ayuda General</a></li>
		      <li class="divider"></li>
		      <li><a href="#"><i class="icon-off"></i>Cerrar sesión</a></li>
		  </ul>
		</li>
	      </ul>
	    </div>
	  </div>
	</div>
      </div>
    </div>
    <br/>