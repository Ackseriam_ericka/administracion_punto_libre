<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Consulta de Bien</h3>
	    <div class="row-fluid">
	      <div class="span12 text-center btn-primary">
		<span>Consulta por Tipo</span>
	      </div>
	    </div><br />
	    <div class="row-fluid text-center" title="Elija el tipo de bien que desea consultar">
	      <div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Equipos de Computacion<span class="caret"></span></button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="consultarpc.php">PC</a></li>
		  <li><a href="consultar_componente.php">Componente Interno</a></li>
		  <li><a href="consultarperiferico.php">Periférico</a></li>
		</ul>
	      </div><!-- /btn-group -->
	      <a class="btn btn-primary" href="consultarmobiliario.php">Mobiliario</a>
		<div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Insumo<span class="caret"></span></button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="consultar_insumooficina.php">Insumo de Oficina</a></li>
		  <li><a href="consultar_insumolimpieza.php">Insumo de Limpieza</a></li>
		</ul>
	      </div><br><br><hr>
	    </div>
	  </div>
	</div>
      </div>
    </div>
  <?php
  require('piepagina.php');
  ?>