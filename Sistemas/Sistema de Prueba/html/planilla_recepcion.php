<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
<div class="span9">
  <div class="hero-unit">
    <h3 class="text-center"> PLANILLA DE REGISTRO DE BIENES</h3>
      <form method="post" action="insertar_registro.php">
        <div class="row-fluid">
	  <div class="span12 text-center btn-primary">
	    <span>Registro de Ubicación</span>
	  </div>
	</div><br>
	<div class="row-fluid">
	  <div class="span3">
	    Fecha de Registro:
	  </div>
        <div class="span4">
	<input type="text" id=""  name="fecha_registro" value="<?php echo (date('Y/m/d'))?>"/><br/>
	</div>
   </div>
       
	</div><br>
	<div class="row-fluid">
	  <div class="span1" align="right">
	    Estado:
	  </div>
	  <div class="span2">
	    <select name="idpuntolibre">
             	<option value="" >Mérida</option>
	    </select>
	  </div>
	  
	  <div class="span1" align="right">
	     Ciudad:
	  </div>
	  <div class="span2">
	    <select name="idpuntolibre">
		<option value="">Mérida</option>		    	
		<option value="">Apartaderos</option>
		<option value="">Bailadores</option>
		<option value="">Ejido</option>
		<option value="">El Vigía</option>
		<option value=""selected >Mérida</option>
		<option value="">Mucuchies</option>
		<option value="">Palmarito</option>
		<option value="">Santo Domingo</option>
		<option value="">Timotes</option>
	    </select>
	  </div>
	  <div class="span2" align="right">
	     Ubicación:
	  </div>
	  <div class="span2">
	    <select name="idpuntolibre">
	    	<option value="">Alberto Adriani </option>
		<option value="">Andrés Bello </option>
		<option value="">Anzoátegui</option>
		<option value="">Antonio Pinto Salinas </option>
		<option value="">Arzobispo Chacón  </option>
		<option value="">Campo Elías </option>
		<option value="">Caracciolo Parra Y Olmedo</option>
		<option value="">Cardenal Quintero </option>
		<option value="">Guaraque</option>
		<option value="">Julio Cesar Salas</option>
		<option value="">Justo Briceño</option>
		<option value=""selected >Libertador</option>
		<option value="">Obispo Ramos De Lora</option>
		<option value="">Padre Noguera </option>
		<option value="">Pueblo Llano </option>
		<option value="">Rangel </option>
		<option value="">Rivas Dávila </option>
		<option value="">Santos Marquina </option>
		<option value="">Sucre </option>
		<option value="">Tovar </option>
		<option value="">Tulio Febres Cordero </option>
		<option value="">Zea</option>
	    </select>
	  </div>
        </div><hr>
	<div class="span12 text-center">
	  <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i>Siguiente</button> 
	</div>
      </form>
</div>
</div>
</div>
</div>
<?php
  require('piepagina.php');
?>

