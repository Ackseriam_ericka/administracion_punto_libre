	<div class="footer-azul">
            <div class="container">
		<div class="row-fluid">
		    <div class="span12">
			<div class="span3">
			    <a href="http://puntolibre.gnu.org.ve/"><img src="../img/punto-small.png"/> Punto Libre 2013 <i class="icon-globe"></i></a>
			</div>
			<div class="span6">
			    Algunos derechos reservados <img alt="" src="../img/Copyleft.png" width='15' height='15'/></a>
			    <br /> Esta constituido bajo una licencia de <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.es" title="Creative Commons Attribution 3.0 Unported">Creative Commons Attribution 3.0 Unported</a>.<br />
			</div>
			<div class="span3">
			    <p class="muted pull-right"><a href="http://gnu.org.ve/" target="_blank"><img src="../img/gnu.png" width='15' height='15'/> Proyecto GNU Venezuela 2013 <i class="icon-globe"></i> </a></p>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </body>
</html>