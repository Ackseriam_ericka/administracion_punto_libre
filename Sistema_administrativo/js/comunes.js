
/*
    function ev (x,y,z)
    
    x es el elemento sobre el que se quiere establecer la escucha,
    y el evento
    z la función a ejecutar 
*/
function ev (x,y,z) { 
        if (document.addEventListener){
            //console.log(x);
            
            /*
             
             addEventListener ejecuta la creación de la escucha para los eventos
            */
            x.addEventListener(y,z,true);
            
        } else {
                
        /*
        
         Hace lo mismo que addEventListener pero en Internet Explorer
        */
            x.id.attachEvent('on'+y,z); 
        }
    }
    

function crearCookie(Name, Value, Expiration){
  
        //Crear una variable de fecha que contiene
             // La fecha de caducidad
            var ExpDate = new Date();
            if (Expiration != null)
             ExpDate.setDate(ExpDate.getDate() +
                     Expiration);
           
            // Encode the data for storage.
            var CookieValue = escape(Value) +
             "; expires=" + ExpDate.toUTCString();
            
            // Store the cookie.
            document.cookie = Name + "=" + CookieValue;
    }
    
function obtener_cookie(Name){
      
      //Obtener todas las cookies y división
   //Ellos en galletas individuales.
  var Cookies=document.cookie.split(";");
 
  // Procesar cada cookie a su vez.
  for (var i=0; i<Cookies.length; i++)
  {
        // Obtener el nombre de la cookie.
        var CName = Cookies[i].substr(0,
          Cookies[i].indexOf("="));
       
        // Obtener el valor de la cookie.
        var CValue = Cookies[i].substr(
          Cookies[i].indexOf("=") + 1);
        
       //Reemplazar cualquier caracteres de escape.
      
        CName = CName.replace(/^\s+|\s+$/g, "");
       
      // Si el nombre de la cookie coincide con el
         // Nombre que fue aprobada por la persona que llama, el retorno
         //La cookie asociada.
        if (Name == CName)
        {
          return unescape(CValue);
        }  
   }

        return null;
}



function destruir_cookie(Name){
        
        return document.cookie = Name +'=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}
        
