
var seleccionados= new Array();


/*
 Activa las escuchas con la funcion ev de comunes.js para todos los botones
 input 
*/


var botones;
var botones_habilitar= new Array();
var botones_deshabilitar= new Array();
var identificadores_punto_libre= new Array();




function seleccionar(){
        
    
        
        /*Si seleccion los agrego a seleccionados si los desmarco, los saco.
        
        Cuando la cantidad de seleeccionados sea cero desbloqueo    
        */
      
        obj=this;
        if(obj.checked){
            
            //metodo para agregar elementos a un arreglo
            seleccionados.push(obj);   
         
            
        }else{
            //funcion desarrollada para extraer elementos del arreglo
            remove_element(seleccionados,obj)

    
        }
        
   
        
        if(seleccionados.length>0){
            //Desactiva los botones habilitar cuando los seleccionados son mayores que cero
            desactivar_activar(botones_habilitar,true); // si es verdadero aplica el disabled
            
        }else{
            //Activa los botones habilitar cuando la condicio anterior es falsa
            desactivar_activar(botones_habilitar);
            
        }
        
        /*
        Tienen que agregar aqui condiciones similares para los botones deshabilitar
        
        */
        
        
    }
    /*Esta funcion crea un arreglo con todos los identificadores de los
    puntos libres, tiene como objetivo transformarlos posteriormente en una cadena
    para pasarla a la cookie
    la x tiene un evento recuerda
    */
    
    function crear_arreglo_ids(){
        
        for(i=0;i<seleccionados.length;i++){
        
            id = seleccionados[i].id.split("_")[1];
            identificadores_punto_libre[i] = id;
        //ojo
        }
        
    }
    
    /*
    
    Esta funcion usa el arreglo anterior, lo convierte en una cadena
    
    */
    
    
    function generar_cadena_cookie(){
        
        cadena = identificadores_punto_libre.join();
        return cadena;
        
    }

    
    
    function procesar_deshabilitacion(){
        
        crear_arreglo_ids();
        if(seleccionados.length==0){
            
            alert("No ha seleccionado ninguna opcion");
            return;
        }
        cadena=generar_cadena_cookie();
        
        crearCookie("deshabilitar_puntos_libres", cadena, 3600)
        //alert(cadena);
        window.location.href="deshabilitacion.php";
        return;
        
    }
    
    function remove_element(arreglo, obj){
        longitud=arreglo.length;
        
        pos=null;
        for(i=0; i<longitud; i++){
            if(arreglo[i].id==obj.id){
                pos=i;
                break;
            }
        }
        if(pos!=null){
            seleccionados.splice(pos, 1);
            return pos;
        }else{
            
            return null
        }
    }
    
    
    function mostrar_seleccionados(){
        /*
            Función de depuración
        */
        cadena="";
        for(i=0; i<seleccionados.length;i++){
            alert(i);
            cadena=cadena+seleccionados[i].id+"\n";
            
            
        }
        alert(cadena);
        
    }
    
    
    
   
    function agrupar_botones(){
        /*
        
        A partir de los input los agrupa en botones habilitar y deshabilitar
        al dividir el nombre
        
        */
        
        botones_input=document.getElementsByTagName("input");
        
        for(i=0; i<botones_input.length; i++){
            
            nombre_completo_boton=botones_input[i].name; 
            tipo_boton=nombre_completo_boton.split("_")[0];
            if(tipo_boton=="habilitar"){
                
                botones_habilitar.push(botones_input[i]);
                
            }else if(tipo_boton=="deshabilitar"){
                
                botones_deshabilitar.push(botones_input[i]);
                
            }
            
            
        }
        
    }
    
    
    
    function desactivar_activar(botones, modo){
        
        /*
    
        Al activar los botones, se debe pasar el argumento de botones
        en dado caso de pasar el segundo argumento debe ser false, con tal
        desactivar los mensajes. 
    
        */    
        
        modo = (modo) ? modo : false;
        
        for(i=0;i<botones.length;i++){
            
            botones[i].disabled=modo;
            
        }
        
    }