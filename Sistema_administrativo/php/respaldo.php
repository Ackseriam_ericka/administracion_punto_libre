<?php
  session_start();
  include("../conf.php");
  include("lib/conexion.php");
  $con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true) {

?>
  <!DOCTYPE html>
<html lang="es">
 <head>
    <meta charset="utf-8" />
    <title>Sistema Administrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" media="screen"/>  
<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
  </head>
 <body>

 
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown active"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i>Administración<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="../html/permisologia.php"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="../html/consultausuarios.php"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="../html/consultausuarios.php"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="../html/consulta_general.php"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="../html/menu_crear_punto.php"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar Puntos Libres</a></li>
                           <li><a href="../html/ayuda.php"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="icon-user icon-white"></i> <?php echo    $_SESSION['usuario']; ?> <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                          <li><a href="../html/perfil.php"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
               </div>
                  </div>
                </div>
              </div>
            </div> 
            


       <br>
            <br>
          <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li title="Iras a la pantalla principal del sistema"><a href="../html/menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
<div class='span9'>
	<div class='hero-unit'>
		<div class='row-fluid'>
                    <div class='span12 text-center btn-primary'>
			<span>Respaldo y Restauracion de Base de Datos del Sistema</span>
                        </div>
			
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Respaldar la información registrada en el Sistema</span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
<input type=button value='Respaldar' class="btn btn-primary" TITLE='respalda base de datos del sistema' onClick=location.href='../respaldos/respaldar.php?act=respaldar'>
				</div>
			</div>
                        <br>
			<div class='row-fluid'>
				<div class='span12 text-center btn-primary'>
				    <span>Restaurar Información</span>
				</div>
			</div>
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Restaurar la información con un archivo de respaldo guardado</span>
				</div>
			</div>
			<form class="text-center" action='../respaldos/respaldar.php?act=aplicarresp' method='post' enctype='multipart/form-data'>
<input name='archivo' type='file' size='20'/>
<input name='enviar' type='submit' class="btn btn-primary" TITLE='restaura base de datos del sistema' value='Restaurar' />
<input name='action' type='hidden' value='upload' />     
			</form>
		</div>
	</div>
</div>
</div>
</div>
              <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
  <script type="text/javascript">
  ${"dropdown-toggle"}.dropdown{}
         
    </script>
  

  
    <script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>
<?php

}else{
  
  header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>




