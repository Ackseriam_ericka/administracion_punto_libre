<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>configuracion de correo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
      <?php
        include("cabecera.php")
      ?>                      
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                              <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.html"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.HTML" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.HTML"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.HTML"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status de los  P.L</a></li>
                        
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
                	 <div class="span9">
                          <div class="hero-unit">
                              <h2 class="text-center">Perfil</h2>
                         <form method="post" action="">
                           <div class="row-fluid">
                              <div class="span12 text-center btn-primary  ">
                                <span>Configuración de tu cuenta</span>
                              </div>
                           </div>
                           <table class="table table-striped" >
                                   </tr>
                                     <tr>
                                        <th colspan="8"><h4>Nombre completo:</h4></th>
                                          
                                          <td colspan="8"></td>
                                          <th >
                                             <div class="btn-group">
                                                 <a class="btn btn-primary" href="configuracion_nombre.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                              </div>
                                         </th> 
                                  </tr>
                                  <tr>
                                        <th colspan="8">Nombre de usuario:</th>
                                        
                                        <td colspan="8"></td>
                                        <td>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_nombre_usuario.HTML"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </td> 
                                  </tr>

                                  </table>
                     <div class="span8">
                          <div class="hero-unit">
                            <form method="post" action="nombre_perfil.php">
                                 <h5 class="text-info">Correo Electronico</h5>
                             <table align="center">
                                    <tr>
                                  
                                         <td colspan="1"><h5>Correo electronico:</h5></td>

                                       <td><input type="text" id="inputEmail" placeholder="ericka.simanca@gmail.com" onfocus="if(this.value=='Ejemplo@gmail.com')this.value=''" size="22"disabled required/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8"> <small>
                                               Agregar otra cuenta de correo<cite ></cite></small>
			                                    </blockquote>
			                            </td>
                                    </tr>
                                    <tr>
                                        <td><h5>
                                         Nueva dirección de correo:</h5>
                                        </td>
                                        <td><input type="text" id="inputEmail" placeholder="Ejemplo@gmail.com" onfocus="if(this.value=='Ejemplo@gmail.com')this.value=''" size="22"/></td>


                                    </tr>

                                    <tr>
		                                    <td colspan="8">
			                                    <small>Para guardar la actualización de tu perfil debes introducir la clave del sistema<cite ></cite></small>
			                                    </blockquote>
		                                    </td>
                                   </tr>
                                      <tr>

                                          <td><h5>Introduzca Clave:</h5></td>
                                           <td><input type="password" name="pass" size="22" title="Debe contener 7 caracteres como maximo"></td>

                                      </tr>
                           </table>
                         </form>

                          </div>
                           </div>

                           <table class="table table-striped" >

                              
                                
                                    <tr>
                                        <th colspan="8">Estado:</th>
                                        
                                        <td colspan="8
                                        "></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_estado.html"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>
                                   <tr>
                                        <th colspan="8">Dirección:</th>
                                        
                                        <td colspan="8
                                        "></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_direccion.HTML"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>
                                   <tr>
                                        <th colspan="8">Numero de Telefono:</th>
                                        
                                        <td colspan="8
                                        "></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_telefono.HTML"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>

                              </table>
                       </form>
                     </div>
                 </div>

  
     <div id="pie">
            Todos los Derechos Reservados al Punto Libre
        </div> 
      
     
            
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>


</body>
</html>

