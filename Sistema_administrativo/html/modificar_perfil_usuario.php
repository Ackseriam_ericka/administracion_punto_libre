 <?php
session_start();
include("../conf.php");
include("../php/lib/conexion.php");
$con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Asignar_Permisos']==1)
{
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>permisologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
       <?php
        include("../html/cabecera.php")
      ?>                               
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                            <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status del P.L</a></li>
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>


    


 <div class="span9">
    <div class="hero-unit">
      <h3 class="text-center">Administración General</h3>
      
        <div class="row-fluid">
          <div class="span12 text-center btn-primary  ">
                <span>Definición de perfiles de usuario</span>
         </div>
         </div>
       <form  action="../php/permiso_usuario.php" method="post"
      	  
	  <br>
	  <br>
	  
        	      <?php
		      
		
	     
	      $_CONSUL=array();
	      
	      $cedula=$_POST['cedula'];
	  
	       $consul= "SELECT cedula FROM personas   WHERE (cedula='$cedula')";
	       $resp=mysql_query($consul);
	       if(!$resp){
		  //Error en la consulta
		  echo "Error en la consulta: ".mysql_error();
	       }else if(mysql_numrows($resp)==0){
		  
		  //No coincide el usuario y la contraseña
		//  $_CONSUL['error']="Cedula no encontrada en la base de datos";
		 
		// header("Location:". $_CONF['server_web'].$_CONF['app']."html/consultausuarios.php");
		   echo "<script type=text/javascript>
				    alert(' Cédula no existente en la base de datos. Presiona aceptar para continuar');
				    document.location=('../html/permisologia.php');
				</script>";
	    }else{
		    
		    
		    $registro=mysql_fetch_assoc($resp);
		    $_CONSUL['cedula']=True;
		    if($_CONSUL['cedula']==True)
		    
		    $_CONSUL['cedula']=$cedula;
	      
              
		
		 $orden="SELECT * FROM personas, nivel_usuario

		 WHERE personas.idpersonas=nivel_usuario.usuarios_idusuarios and cedula='$cedula'";
		 $listado = mysql_query ($orden,$con);
		 
		 echo mysql_error();
           if ($lista= mysql_fetch_array($listado));
	   
				              
			echo "<table class='table table-bordered table-striped' align=center>";
			echo"<tr class='success'><td>nombres: </td><td>".$lista["nombres"]."</td></tr>
				  <tr><td>apellidos:  </td><td>".$lista["apellidos"]."</td></tr></table>";
   

			
            }	
			  
        ?>
	
	  
	<table  align="center" cellpadding="10">
      			
                    <tr>
      				  <td><h5><input type="checkbox" name="opcion1" value="1"<?php if($lista['Insertar_Informacion']) echo "checked";?>/>Insertar Información en el Sistema</h5></td>
      				  <td><h5><input type="checkbox" name="opcion2" value="1"<?php if($lista['Modificar_Informacion']) echo "checked";?>/>Modificar Información en el Sistema<h5></td> 
      				  <td><h5><input type="checkbox" name="opcion3" value="1"<?php if($lista['Deshabilitar_Habilitar']) echo "checked";?>/>Deshabilitar o Habilitar </h5></td> 
      		      </tr>
      		      <tr>

                        <td><h5><input type="checkbox" name="opcion4" value="1"<?php if($lista['Eliminacion']) echo "checked";?>/>Eliminación</td> </h5>
                        <td><h5><input type="checkbox" name="opcion5" value="1"<?php if($lista['Consultar_Informacion']) echo "checked";?>/>Consultar Información en el Sistema </td> </h5>
                        <td><h5><input type="checkbox" name="opcion6" value="1"<?php if($lista['Asignar_Permisos']) echo "checked";?>/>Asignar Permisos de Usuario</td> </h5>
                      </tr>
                    <tr>
                      
                        
                  
                       
                    
                        <td><h5><input type="checkbox" name="opcion7" value="1"<?php if($lista['Acceso_inventario']) echo "checked";?>/>Acceso al sistema de inventario </td> </h5>
                        <td><h5><input type="checkbox" name="opcion8" value="1"<?php if($lista['Acceso_Cursos']) echo "checked";?>/>Acceso  al sistema de Cursos</td></h5>
                          <td><h5><input type="checkbox" name="opcion9" value="1"<?php if($lista['Acceso_Soporte']) echo "checked";?>/>Acceso al sistema de Soporte</td></h5>   
                    </tr>
        
             
                        </table>
                    
                    



      </table> 
      <div class="row-fluid">
                <div class="span12 text-center">
                  <div class="btn-group">
                    <input type="hidden" name="idusuario" value="<?php echo $lista['usuarios_idusuarios'];?>"/>  
            <button type="submit"   class="btn btn-primary" ><i class="icon-file icon-white"></i> Guardar</button>
                       <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
                   </div>
                </div>
      </div>
                      
      </table>
      </align>
</form>
         
              <script type="text/javascript" src="../js/bootstrap.js"></script>
              <script type="text/javascript" src="../js/jquery.js"></script>
             <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
              
             
            

      <script type="text/javascript">
      ${"dropdown-toggle"}.dropdown{}
             
        </script>

        <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
      <script type="text/javascript">
          $('#myModal').on('hidden', function () {
          // do something…
          })</script>

      </body>
      </html>
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
    
}
?>

