<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>configuracion de Telefono</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="#"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i> Administración de P.L.<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="permisologia.php"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="consultausuarios.php"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="consultausuarios.php"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="#"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="menu_crear_punto.php"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar y Status de los Puntos Libres</a></li>
                   
                        
                          <li><a href="ayuda.php"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li  class="dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil.php"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                                
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
                        
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
                	 <div class="span9">
                          <div class="hero-unit">
                              <h2 class="text-center">Perfil</h2>
                         <form method="post" action="">
                           <div class="row-fluid">
                              <div class="span12 text-center btn-primary  ">
                                <span>Configuración de tu cuenta</span>
                              </div>
                           </div>
                           <table class="table table-striped" >
                                   </tr>
                                     <tr>
                                        <th colspan="8"><h4>Nombre completo:</h4></th>
                                          
                                          <td colspan="8"></td>
                                          <th >
                                             <div class="btn-group">
                                                 <a class="btn btn-primary" href="configuracion_nombre.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                              </div>
                                         </th> 
                                  </tr>
                                  <tr>
                                        <th colspan="8">Nombre de usuario:</th>
                                        
                                        <td colspan="8"></td>
                                        <td>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_nombre_usuario.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </td> 
                                  </tr>
                                   <tr>
                                        <th colspan="8">Correo Electronico:</th>
                                        
                                        <td colspan="8"></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_correo.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>
                                   <tr>
                                        <th colspan="8"><h4>Estado:</h4></th>
                                        
                                        <td colspan="8
                                        "></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_estado.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>
                                  <tr>
                                        <th colspan="8">Dirección:</th>
                                        
                                        <td colspan="8
                                        "></td>
                                        <th>
                                           <div class="btn-group">
                                               <a class="btn btn-primary" href="configuracion_direccion.php"><i class="icon-user icon-pencil icon-white"></i>Editar</a>
                                            </div>
                                       </th> 
                                  </tr>

                                  </table>
                                  </form>
                    
                            <form method="post" action="nombre_perfil.php">
                                 <h5 class="text-info">Numero de telefono:</h5>
                             <table align="center">
                                    <tr>
                                  
                                         <td colspan="1"><h5>
                                         Numero de telefono actual:</h5></td>

                                       <td><input type="text" id="telefono" placeholder="04161328321"  size="22"/>
                                        </td>
                                    </tr>
                            
                                    <tr>
                                        <td><h5>
                                        Nuevo telefono:</h5>
                                        </td>
                                        <td colspan="6">
                                        <textarea title="Solo caracteres" name="telefono" cols="25" rows="5"></textarea>
                                    </tr>

                                    <tr>
		                                    <td colspan="8">
			                                    <small>Para guardar la actualización de tu perfil debes introducir la clave del sistema<cite ></cite></small>
			                                    </blockquote>
		                                    </td>
                                   </tr>
                                      <tr>

                                          <td><h5>Introduzca Clave:</h5></td>
                                           <td><input type="password" name="pass" size="22" title="Debe contener 7 caracteres como maximo"></td>

                                      </tr>
                           </table>
                         </form>

                  
                     </div>
                 </div>

  
     <div id="pie">
            <align="center">Todos los Derechos Reservados al Punto Libre
        </div> 
      
     
            
        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>


</body>
</html>

