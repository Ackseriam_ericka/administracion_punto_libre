
<?php
session_start();
include("../conf.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true){  ?>
<!DOCTYPE html>
  

<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>consulta de usuario</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
      
  <script type="text/javascript">
  
  /*
function validar_cedula(cedula) {
    
    
    var expreg= /^[JV]-\d{8}-\d$/;
    if (!expreg.test("cedula")) {
         alert("El texto no tiene el formato esperado");
        
    
    }
    alert('Formato de cedula permitido')
    
}
 

</script>
  </head>

  <body>
       <?php
        include("cabecera.php")
      ?>                      
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active"><a href="#"><i class="icon-home icon-white"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                            <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>Modificacion y Status de los P.L</a></li>
                            
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
                     <div class="span9">
            <div class="hero-unit">
                <h3 class="text-center">Modificar perfil</h3>
                
                <div class="row-fluid">
                    <div class="span12 text-center btn-primary  ">
                          <span>Introduce tu cedula</span>
                 </div>
                 </div>
    

    <br>
    
  <table id=table align="center"> 

  <form name="cedula" action="../html/perfil1.php" METHOD="POST" onsubmit="return validar_cedula(this);">  
         
           <tr>
             <th><h5>Cedula de Identidad:</h5></th>
                <td><input type="text" placeholder="XXXXXXXX" name="cedula" class="input-medium search-query" required></td>
            
             <td><button class="btn btn-primary" TYPE="SUBMIT" data-loading-text="Loading" >Consultar</button><br></td>
       
  
             <td></td>
          </tr>
                  </table>
                  </form>
                  <br>

           
                          
              <script type="text/javascript" src="../js/bootstrap.js"></script>
              <script type="text/javascript" src="../js/jquery.js"></script>
             <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
              
             
            

      <script type="text/javascript">
      ${"dropdown-toggle"}.dropdown{}
             
        </script>

        <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
      <script type="text/javascript">
          $('#myModal').on('hidden', function () {
          // do something…
          })</script>
      </body>
      </html>

<?php

}else{
  
  header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>



