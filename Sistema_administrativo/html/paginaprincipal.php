<?php
session_start();
include("../conf.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
   <meta charset="utf-8"/>
  <title>Pagina principal</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
</head>
 <body>

  
  <div class="navbar navbar-static-top">
   <div class="navbar-inner">
          <div class="container-fluid">
                 <img src="../img/imagen1.png" alt="primera imagen"  class="img-rounded" align=left width=100 height=100 >
                 <img src="../img/imagen2.png" alt="segunda imagen"  class="img-rounded"
                  align=right width=90 height=90>
    <span class="text-center">
      <h2> Punto Libre GNU LINUX de Venezuela</h2>
    </span>
    <span class="text-center">
      <h5>Organizacion encargada de impulsar el software libre en Venezuela</h5>
    </span>
    </div>
   </div>
  </div>
    <div class="span8">
      <div class="hero">
                                     <h3 class="text-center text-info">  Iniciar Sesi&oacute;n</h3>
                                    </div>
                                    </div>

                        <div class="span4">

                   <table align="center">
                           <br>
                           <br>
                           <br>
                           <br>


                   <form action="<?php echo $_CONF['server_web'].$_CONF['app']."php/iniciar_sesion.php" ?>" method="POST"> <h3 class="text-center text-info">
		  <?php if (array_key_exists("msg", $_SESSION)){ ?>
		   <tr>
		    <td><span class="label label-success"><?php echo $_SESSION['msg'];  ?></span> </td>-
		   </tr>
		  <?php } elseif(array_key_exists("error_msg", $_SESSION)) { ?>
		  
		  <tr>
		     <td><span class="label label-important"><?php echo $_SESSION['error_msg'];  ?></span> </td>
		   </tr>
		  
		  <?php }
		  
		  session_destroy();
		  $_SESSION=array();
		  ?>
				       <tr>
                                          <td><p class="muted"> Usuario:</p></td>   
                                          <td><input type="text" name="usuario" required placeholder="Nombre de usuario o Email" title="Intoducir solo caracteres"/>	</td>
                                    	 </tr>
                                       <tr>
                                    	       <td>	<p class="muted">Contrase&ntilde;a: </p></td>
                                             <td><input type="password"  required placeholder="contraseña" name="clave"title=" Debe contener 7 caracteres"/> </td>
                                        </tr>
                              </div>
                           </div>
                                  <tr>
                                       <td>
                                          <button class="btn btn-primary" type="submit">Iniciar sesi&oacute;n</button>
					  
                                          
                                         </td>
                                  </tr>
                       
                   </table>
             </form>
                   <p ALIGN="center"> <a href="Registro.php">Registrate aqui</a></p>
                   <p ALIGN="center"> <a href="olvidastes_contrasena.php">Olvidastes tu contrase&ntildea?</a></p>
                   </div>
                 
          </div>
          <script type="text/javascript" src="../js/jquery.js"></script>
          <script type="text/javascript" src="../js/bootstrap.js"></script>
  </body>
</html>
