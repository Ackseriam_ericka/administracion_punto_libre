<?php
session_start();

if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true){  
  
  ?>

<!DOCTYPE html>
<html xml:lang="es">
	<head>
		<meta charset="utf-8" />
		<title>Crear punto</title>
 		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
   	    <meta name="description" content="">
        <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
        <link href="../css/bootstrap-responsive.css" rel="stylesheet">


  </head>
	  <body>
      <?php
        include("cabecera.php")
      ?>                                               
            <br>
            <br>
<!--
 <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li ><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li  title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li  title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li class="active"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
			                       <li title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status del P.L</a></li>
			              <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>-->

<div class="span12">
    <div class="hero-unit">

					<h3 class="text-center">Ubicación de los Puntos Libres</h3>
					<div class="row-fluid">
					   <div class="span13 text-center btn-primary ">
				                <span>Definición de perfiles de usuario</span>
				         </div>
				         </div>
					    
						<table class="table table-striped" border=4 bordercolor="#0080FF" cellpadding=2 align="center">
									<tr>
							<td> Estado </td>
							<td> Dirección </td>
							<td> Mapa </td>
							<td> Status </td>
							</tr>
							<?php
							
							include("../php/ubicacion_de_los_puntos.php");
							if (isset($error)) echo $error;
							for($i=0; $i<count($array_salida);$i++){ ?>
								<tr>
								<td> <?php echo $array_salida[$i]["estado"]; ?> </td>
								<td> <?php echo $array_salida[$i]["direccion"]; ?></td>
								<td> <img src="<?php echo $_CONF['server_web'].$_CONF['app']."php/cargar_imagen.php?id_punto=".$array_salida[$i]["idpunto_libre"];?>"
									  alt="<?php echo $array_salida[$i]["estado"]; ?>"
									  width="400"
									  height="400"/></td>
									  
								<td><?php echo $array_salida[$i]["situacion_actual"]; ?></td>
								</tr>
								
								
								
						<?php } ?>
						</table>
  </div>
        </div>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>

</body>
</html>
<?php

}else{
  
  header("Location:".$_CONF['server_web'].$_CONF['app']."paginaprincipal.php");
  
}
?>

