<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>ayuda</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i> Administración de P.L.<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="permisologia.php"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="consultausuarios.php"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="consultausuarios.php"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="#"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="menu_crear_punto.php"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar y Status de los Puntos Libres</a></li>
                         
                        
                          <li><a href="ayuda.html"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li  class="dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil.php"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.php" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                                
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
				
				<div class="span9">
					<div class="hero-unit">
						<h2 class="text-center">Ayuda</h2>
                                                <a id="top"></a>
						<p>Aqui tienes la principales Preguntas y respuestas de como usar el Sistema de Administractivo de Punto Libre.
						</p>
				<div class="navbar ">
			<div class="navbar-inner">
				<div class="container-fluid">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
					 </button>
					<span class="brand">Menu de Ayuda</span>
					<div class="nav-collapse collapse">
						<ul class="nav ">
								<li class="dropdown">
								<a href="registrosoporte.php" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-question-sign"></i> Preguntas Frecuentes <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li ><a href="#p1" ><i class="icon-question-sign"></i> ¿Como creo un Punto Libre?</a></li>
									<li><a href="#p2"><i class="icon-question-sign"></i> ¿Como puedo Ingresar a los modulos por medio del sistema?</a></li>
									<li><a href="#3"><i class="icon-question-sign"></i> ¿Como puedo notificar un determinado inconveniente Punto Libre?</a></li>
								</ul>
							</li>
								<li><a href="#"><i class="icon-book"></i> Manual del Usuario</a></li>
							<li><a href="creditos.php"><i class="icon-bookmark"></i> Creditos</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
<br><br>
						<h3>Preguntas Frecuentes</h3>
						<div class="well">
								<a id="p1"></a>
						<p><h4>¿Como creo un Punto Libre?</h4></p>
						<p>Respuesta:
						<br>
						Para la creacion de un Punto Libre al ingresar al sistema debes dirigirte al menu de administracion general que se en euntra a la izquierda y buscar la opcion "Crear punto",aparecera una ventana que te indicara los datos necesarios que necesitas para poder crearlo 
						¿Fue de ayuda la respuesta?
						<br>
						<div class="btn-group">
							<button class="btn btn-primary" name="ayuda">Si</button>
							<button class="btn btn-danger" name="ayuda">No</button>
						</div>
						<a href="#top" class="pull-right"><i class="icon-arrow-up"></i> Volve al Principio</a>
					</div>
						<div class="well">
								<a id="p2"></a>
						<p><h4>¿Como puedo Ingresar a los modulos por medio del sistema?</h4></p>
						<p>Respuesta:
						<br>
						Para poder tener informacion sobre los modulos que posee el sistema deberas dirigirte a la barra superior donde observaras los modulos,luego presionar el enlaze que desees
						</p>
						¿Fue de ayuda la respuesta?
						<br>
						<button class="btn btn-primary" name="si">Si</button>
						<button class="btn btn-danger" name="no">No</button>
								<a class="pull-right" href="#top"><i class="icon-arrow-up"></i> Volve al Principio</a>
						</div>
					<div class="well">
								<a id="p3"></a>
						<p><h4>¿Como puedo notificar un determinado inconveniente Punto Libre?</h4></p>
						<p>Respuesta:
						<br>
						Para poder notificar algun inconveniente relacionado con el Punto Libres, deberas dirigirte a la barra superior donde observaras la opcion "Administracion de P.L",al presionarla aparecera un menu desplegable,te saldra la opcion "Reportes", mostrara un ventana que te indicara los datos necesarios para enviar un reporte
						</p>
						¿Fue de ayuda la respuesta?
						<br>
						<button class="btn btn-primary" name="si">Si</button>
						<button class="btn btn-danger" name="no">No</button>
								<a href="#top" class="pull-right"><i class="icon-arrow-up"></i> Volve al Principio</a>
					</div>
				</div>
				</div>
				
			</div>
			
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="span8">
					<p class="muted"><a href="http://puntolibre.gnu.org.ve/" target="_blank"><img src="../img/punto-small.png"/> Punto Libre 2013 <i class="icon-globe"></i></a></p>
				</div>
				<!--<div class="span4">
					<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">
					<img alt="Licencia de Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a>
					<br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.es_ES">licencia de Creative Commons Reconocimiento-CompartirIgual 3.0 Unported</a>.
				</div>-->
				<div class="span4">
					<p class="muted pull-right"><a href="http://gnu.org.ve/" target="_blank"><img src="../img/gnu-small.png"/> Proyecto GNU Venezuela 2013 <i class="icon-globe"></i> </a></p>
				</div>
			</div>
		</div>

        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>


</body>
</html>