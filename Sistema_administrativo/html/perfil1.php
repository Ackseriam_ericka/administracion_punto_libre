
<?php
session_start();
include("../conf.php");
include("../php/lib/conexion.php");
$con=conexion();
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true){  

  
      ?>      
      <!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>configuracion de perfil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <script type="text/javascript" src="../js/validadores.js"></script>

  </head>

  <body>
       <?php
        include("cabecera.php")
      ?>                                      
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                         <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                            <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y deshabilitar P.L</a></li>
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
                           <?php
       
       
       
        $_CONSUL=array();
        
        $cedula=$_POST['cedula'];
    
         $consul= "SELECT cedula FROM personas   WHERE (cedula='$cedula')";
         $resp=mysql_query($consul);
         if(!$resp){
      //Error en la consulta
      echo "Error en la consulta: ".mysql_error();
         }else if(mysql_numrows($resp)==0){
      

       echo "<script type=text/javascript>
            alert(' Cédula no existente en la base de datos. Presiona aceptar para continuar');
            document.location=('../html/consultausuarios.php');
        </script>";
      }else{
        
        
        $registro=mysql_fetch_assoc($resp);
        $_CONSUL['cedula']=True;
        if($_CONSUL['cedula']==True)
        
        $_CONSUL['cedula']=$cedula;

         
    
        $sql="SELECT * FROM personas JOIN usuarios ON personas.idpersonas=usuarios.personas_idpersonas
           WHERE cedula='$cedula';";
         
              $result =mysql_query($sql);

              $row =mysql_fetch_assoc($result); //en la variable roww s va almacenar todo en forma de arreglo..   

              //el readonly es para no cambiar el dato almacenado, ejemplo codigo.
                  }  
              ?>

                    <div class="span9">
                       <div class="hero-unit">
                                  <h3 class="text-center">Planilla para Modificar tu perfil</h3>
                                   <table id="tabla">
                                  <form  action="../php/perfil1.php" method="post"> 

        
                                             
                                                     
                                             <div class="row-fluid">
                                               <div  class="span12 text-center btn-primary  ">
                                                                   <span>Modificaciòn</span></div>
                                                    </div>
                                                    <br>
                                                    <br>

                                                     <table align="center">
    
      <tr>
        <td>id personas</td>
        <td><input type="text"  name="idpersonas" readonly  placeholder="VE-0000000"size="22" title="Solo numeros" value="<?php echo $row['idpersonas']?>"</td>
      </tr>
        <tr>
        <td><p>Cedula de Identidad:</p></td> 
       
        <td><input type="text" id="cedula" name="cedula"  placeholder="VE-0000000"size="22" title="Solo numeros" value="<?php echo $row['cedula']?>"></td>
        
      
      </tr>
        <tr>
      <td><p>Nombres:</p></td> 
      <td><input type="text" name="nombre" size="22" title="Solo debe contener caracteres" value="<?php echo $row['nombres']?>"></td> 
      </tr>
        
      <tr>
      <td><p>Apellidos:</p></td>
        <td><input type="text" name="apellido"  size="22" title="Solo debe contener caracteres"  value="<?php echo $row['apellidos']?>"></td>
        </tr>
      
        <tr>
        <td><p>Nombre de Usuario:</p></td> 
        <td><input type="text"  name="usuario"  size="22" title="Solo debe contener caracteres" value="<?php echo $row['login']?>"></td>
        </tr>
       
        <tr>
        <td><p>Corre electronico:</p>
          <td><input type="text" type="email"name="email" value="<?php echo $row['email']?>" /></td>
          </tr>
         
        <tr>
        <td>Numero Telefonico:</td>
        <td><input type="text" id="telefono" name="telefono"  placeholder="solo numeros 0000-0000000"required size="22" title="Solo numeros" value="<?php echo $row['telefono']?>"></td>
        
        
      
        </tr>
        <tr>
        <td><p>Estado de vivienda:</p></td> 
        <td colspan="6">

        <select name="estado">

            <?PHP
           $busq_m=mysql_query("select * from personas where cedula='$cedula'");
           while($reg_m=mysql_fetch_array($busq_m))
              {
                     echo "<option value='".$reg_m['estado']."'>".$reg_m['estado']."</option>";
              }      
           ?>
          <option value="Amazonas">Amazonas</option>
          <option value="Anzoátegui">Anzoátegui</option>
          <option value="Apure">Apure</option>
          <option value="Aragua">Aragua</option>
          <option value="Barinas">Barinas</option>
          <option value="Bolívar">Bolívar</option>
          <option value="Carabobo">Carabobo</option>
          <option value="Cojedes">Cojedes</option>
          <option value="Delta Amacuro">Delta Amacuro</option>
          <option value="Distrito Capital">Distrito Capital</option>
          <option value="Falcón">Falcón</option>
          <option value="Guárico">Guárico</option>
          <option value="Lara">Lara</option>
          <option value="Mérida">Mérida</option>
          <option value="Miranda">Miranda</option>
          <option value="Monagas">Monagas</option>
          <option value="Nueva Esparta">Nueva Esparta</option>
          <option value="Portuguesa">Portuguesa</option>
          <option value="Sucre">Sucre</option>
          <option value="Tachira">Tachira</option>
          <option value="Trujillo">Trujillo</option>
          <option value="Vargas">Vargas</option>
          <option value="Yaracuy">Yaracuy</option>
          <option value="Zulia">Zulia</option>
      </select>
          </td>
          </tr>
          <tr>
          <td><p>Direccion donde se encuentra residenciado:</p></td>
            <td><textarea title="Solo caracteres" name="direccion"   cols="45" rows="5"><?php echo $row['direccion'];?></textarea></td>
           </tr> 

                


          
  </table>
<br/><br/>













    
                                    </table>
                                                             <br>
                                                             <br>
                                       <div class="row-fluid">
                                            <div class="span12 text-center">
                                                 <div class="btn-group">
                                                        <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Cancelar</button>
                                                        <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i> Modificar</button>
                                                 </div>
                                           </div>
                                     </div>
                             </div>          
                        </div>
                     </div>

                   </div> 
               </div>
               
              <script type="text/javascript" src="../js/bootstrap.js"></script>
              <script type="text/javascript" src="../js/jquery.js"></script>
             <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        <script type="text/javascript">
        ${"dropdown-toggle"}.dropdown{}
               
          </script>
        
          <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
        <script type="text/javascript">
            $('#myModal').on('hidden', function () {
            // do something…
            })</script>
        
          <script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
          <script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>
          
      
       
      </form>
  </body>
</html>

<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
      
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>
